﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;

namespace Virtlink.Atlantis
{
	public interface IReplace : IFind
	{
		/// <summary>
		/// Replaces the occurrence by the replacement term.
		/// </summary>
		/// <param name="input">The input in which to replace.</param>
		/// <param name="occurrence">The occurrence to replace.</param>
		/// <returns>A tuple with the resulting string and the occurrence of the replacement;
		/// or <see langword="null"/> when nothing was replaced.</returns>
		Tuple<string, Occurrence> Replace(string input, Occurrence occurrence);
	}
}
