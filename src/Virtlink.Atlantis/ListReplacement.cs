﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System.ComponentModel;
using System.Diagnostics;

namespace Virtlink.Atlantis
{
	public sealed class ListReplacement : IFind, IReplace
	{
		private readonly Dictionary<Regex, string> replacements;

		/// <summary>
		/// Gets the type of query string.
		/// </summary>
		public QueryType Type
		{ get; private set; }

		/// <summary>
		/// Gets whether to match the query string as a separate word.
		/// </summary>
		public bool MatchWholeWords
		{ get; private set; }

		/// <summary>
		/// Gets whether to match the query string's upper and lower case
		/// letters exactly.
		/// </summary>
		public bool DistinguishCapitals
		{ get; private set; }

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="ListReplacement"/> class.
		/// </summary>
		public ListReplacement(IReadOnlyDictionary<string, string> replacements, QueryType type, bool matchWholeWords, bool distinguishCapitals)
		{
			#region Contract
			if (replacements == null) throw new ArgumentNullException(nameof(replacements));
			if (!Enum.IsDefined(typeof(QueryType), type)) throw new InvalidEnumArgumentException(nameof(type), (int)type, typeof(QueryType));
			#endregion

			this.Type = type;
			this.MatchWholeWords = matchWholeWords;
			this.DistinguishCapitals = distinguishCapitals;

			this.replacements = new Dictionary<Regex, string>();
			foreach(var pair in replacements)
			{
				Regex regex = Utils.CreateRegex(pair.Key, type, matchWholeWords, distinguishCapitals);
				if (regex == null)
					throw new InvalidOperationException("Invalid query.");
				string replacement = Utils.CreateReplacement(pair.Value, type);
				this.replacements[regex] = replacement;
			}
		}
		#endregion

		#region IO
		/// <summary>
		/// Reads a replacement list from the specified path.
		/// </summary>
		/// <param name="path">The path.</param>
		/// <param name="trim">Whether to trim excess whitespace from the entries.</param>
		/// <returns>The replacement list.</returns>
		public static ListReplacement ReadFromFile(string path, QueryType type, bool matchWholeWords, bool distinguishCapitals, bool trim)
		{
			#region Contract
			if (String.IsNullOrEmpty(path)) throw new ArgumentException("Path cannot be null or empty.", nameof(path));
			if (!Enum.IsDefined(typeof(QueryType), type)) throw new InvalidEnumArgumentException(nameof(type), (int)type, typeof(QueryType));
			#endregion

			using (var input = File.OpenRead(path))
			{
				return ReadFromFile(input, type, matchWholeWords, distinguishCapitals, trim);
			}
		}

		/// <summary>
		/// Reads a replacement list from the specified stream.
		/// </summary>
		/// <param name="input">The input stream.</param>
		/// <param name="trim">Whether to trim excess whitespace from the entries.</param>
		/// <returns>The replacement list.</returns>
		public static ListReplacement ReadFromFile(Stream input, QueryType type, bool matchWholeWords, bool distinguishCapitals, bool trim)
		{
			#region Contract
			if (input == null) throw new ArgumentNullException(nameof(input));
			if (!input.CanRead) throw new ArgumentException("Input cannot be read.", nameof(input));
			if (!Enum.IsDefined(typeof(QueryType), type)) throw new InvalidEnumArgumentException(nameof(type), (int)type, typeof(QueryType));
			#endregion

			using (var reader = new StreamReader(input, Encoding.UTF8, true, 0x1000, true))
			{
				return ReadFromFile(reader, type, matchWholeWords, distinguishCapitals, trim);
			}
		}

		/// <summary>
		/// Reads a replacement list from the specified reader.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <param name="trim">Whether to trim excess whitespace from the entries.</param>
		/// <returns>The replacement list.</returns>
		public static ListReplacement ReadFromFile(TextReader reader, QueryType type, bool matchWholeWords, bool distinguishCapitals, bool trim)
		{
			#region Contract
			if (reader == null) throw new ArgumentNullException(nameof(reader));
			if (!Enum.IsDefined(typeof(QueryType), type)) throw new InvalidEnumArgumentException(nameof(type), (int)type, typeof(QueryType));
			#endregion

			var replacements = new Dictionary<string, string>();

			var replacement = ReadReplacement(reader, trim);
			while (replacement != null)
			{
				if (replacements.ContainsKey(replacement.Item1))
					throw new InvalidOperationException("Zoekterm komt meerdere malen voor: " + replacement.Item1);
				replacements.Add(replacement.Item1, replacement.Item2);
				replacement = ReadReplacement(reader, trim);
			}

			return new ListReplacement(replacements, type, matchWholeWords, distinguishCapitals);
		}

		/// <summary>
		/// Reads a replacement entry.
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <param name="trim">Whether to trim excess whitespace from the entries.</param>
		/// <returns>The replacement entry; or <see langword="null"/> when the
		/// end of the file has been reached.</returns>
		private static Tuple<string, string> ReadReplacement(TextReader reader, bool trim)
		{
			#region Contract
			Debug.Assert(reader != null);
			#endregion

			string from;
			do { from = reader.ReadLine(); }
			while (from != null && String.IsNullOrWhiteSpace(from));
			string to = reader.ReadLine();

			if (from == null || to == null)
				// End of file.
				return null;

			if (trim) from = from.Trim();
			if (trim) to = to.Trim();

			return Tuple.Create(from, to);
		}
		#endregion

		/// <inheritdoc />
		public Occurrence? FindNext(string input, int offset)
		{
			#region Contract
			if (input == null) throw new ArgumentNullException(nameof(input));
			#endregion
			
			if (String.IsNullOrEmpty(input))
				return null;

			// Find all first matches for all terms.
			var candidates = new List<Match>(replacements.Count);
			foreach(var key in replacements.Keys)
			{
				Match match = key.Match(input, offset);
				if (match.Success)
					candidates.Add(match);
			}

			// We return the earliest, longest, alphabetically first match.
			Match result = candidates
				.OrderBy(m => m.Index)
				.ThenByDescending(m => m.Length)
				.ThenBy(m => m.Value)
				.FirstOrDefault();
			if (result == null)
				return null;

			return new Occurrence(result.Index, result.Length);
		}

		/// <inheritdoc />
		public Tuple<string, Occurrence> Replace(string input, Occurrence occurrence)
		{
			#region Contract
			if (input != null && occurrence.SelectionOffset + occurrence.SelectionLength > input.Length)
				throw new ArgumentOutOfRangeException(nameof(occurrence));
			#endregion

			if (String.IsNullOrEmpty(input))
				return null;
			
			// Find the regex that was responsible for the occurrence.
			var candidates = new List<Tuple<Match, Regex>>(1);
			foreach (var key in replacements.Keys)
			{
				Match match = key.Match(input, occurrence.SelectionOffset, occurrence.SelectionLength);
				if (match.Success)
					candidates.Add(Tuple.Create(match, key));
			}
			Regex regex = candidates
				.OrderBy(m => m.Item1.Index)
				.ThenByDescending(m => m.Item1.Length)
				.ThenBy(m => m.Item1.Value)
				.Select(t => t.Item2)
				.FirstOrDefault();
			if (regex == null)
				// Nothing replaced. The regex was not found.
				return null;

			// Replace.
			string output = regex.Replace(input, this.replacements[regex], 1, occurrence.SelectionOffset);

			return Tuple.Create(output, new Occurrence(occurrence.SelectionOffset, occurrence.SelectionLength + (output.Length - input.Length)));
		}
	}
}
