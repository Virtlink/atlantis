﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Virtlink.Atlantis.Properties;

namespace Virtlink.Atlantis
{
	public partial class FileReplace : Form
	{
		IReplace query;

		public FileReplace()
		{
			InitializeComponent();
			UpdateReplaceAllButton();
		}

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			if (dlgOpenFile.ShowDialog(this) == DialogResult.OK)
			{
				txtPath.Text = dlgOpenFile.FileName;
			}
		}
		private void btnReplaceAll_Click(object sender, EventArgs e)
		{
			ReplaceAll();
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void txtPath_TextChanged(object sender, EventArgs e)
		{
			UpdateReplaceAllButton();
		}

		private void UpdateReplaceAllButton()
		{
			btnReplaceAll.Enabled = !String.IsNullOrWhiteSpace(txtPath.Text);
			lblStatus.Text = "";
		}

		private UndoRecord undoRecord;
		private Range selectedRange;
		private int replaceCount = 0;
		private void ReplaceAll()
		{
			this.query = CreateQuery();
			if (this.query == null)
				return;

			// Save the current selection.
			this.selectedRange = Globals.ThisAddIn.Application.Selection.Range;

			EnterLongOperation("Meervoudig vervangen");

			replaceAllWorker.RunWorkerAsync();
		}

		private void replaceAllWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			replaceCount = 0;
			bool success = WordSearch.FindNext(this.query, false, 0);
			while (success && !replaceAllWorker.CancellationPending)
			{
				bool replaceSuccess = WordSearch.Replace(this.query);
				if (replaceSuccess)
				{
					replaceCount++;
					SetStatus(true, "Vervangen: {0} maal.", replaceCount);
				}
				success = WordSearch.FindNext(this.query, false);
			}
		}

		private void replaceAllWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			// Restore the selection.
			this.selectedRange.Select();

			if (!e.Cancelled)
				SetStatus(true, "Vervangen: {0} maal.", replaceCount);
			else
				SetStatus(true, "Geannuleerd. Vervangen: {0} maal.", replaceCount);

			LeaveLongOperation();
		}

		private void EnterLongOperation(string name)
		{
			prbProgress.Visible = true;
			btnCancel.Visible = true;
			pnlMain.Enabled = false;

			this.undoRecord = WordSearch.StartUndo(name);
		}
		
		private void LeaveLongOperation()
		{
			this.Invoke(new Action(() =>
			{
				prbProgress.Visible = false;
				btnCancel.Visible = false;
				pnlMain.Enabled = true;

				WordSearch.EndUndo(this.undoRecord);
			}));
		}

		private void SetStatus(bool success, string str, params object[] args)
		{
			this.Invoke(new Action(() =>
			{
				lblStatus.Text = String.Format(str, args);
				lblStatus.ForeColor = success ? SystemColors.ControlText : Color.Red;
			}));
		}

		private IReplace CreateQuery()
		{
			ListReplacement query;
			try
			{
				query = ListReplacement.ReadFromFile(
					txtPath.Text,
					chkRegularExpressions.Checked ? QueryType.RegularExpression : QueryType.Simple,
					chkWholeWord.Checked,
					chkDistinguishCapitals.Checked,
					chkTrim.Checked);
			}
			catch (InvalidOperationException ex)
			{
				MessageBox.Show(this, "Het vervangingsbestand is ongeldig: " + ex.Message, "Ongeldige opmaak", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return null;
			}
			catch (FileNotFoundException)
			{
				MessageBox.Show(this, "Het vervangingsbestand is niet gevonden.", "Bestand niet gevonden", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return null;
			}
			catch (IOException)
			{
				MessageBox.Show(this, "Er is een onbekende fout opgetreden.", "Onbekende fout", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return null;
			}
			return query;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			replaceAllWorker.CancelAsync();
		}

		private void FileReplace_Load(object sender, EventArgs e)
		{
			LoadSettings();
		}

		private void FileReplace_FormClosing(object sender, FormClosingEventArgs e)
		{
			SaveSettings();
		}

		private void LoadSettings()
		{
			txtPath.Text = Settings.Default.FileReplace_txtPath ?? String.Empty;

			chkRegularExpressions.Checked = Settings.Default.FileReplace_chkRegularExpressions;
			chkWholeWord.Checked = Settings.Default.FileReplace_chkWholeWord;
			chkDistinguishCapitals.Checked = Settings.Default.FileReplace_chkDistinguishCapitals;
			chkTrim.Checked = Settings.Default.FileReplace_chkTrim;
		}

		private void SaveSettings()
		{
			Settings.Default.FileReplace_txtPath = txtPath.Text;

			Settings.Default.FileReplace_chkRegularExpressions = chkRegularExpressions.Checked;
			Settings.Default.FileReplace_chkWholeWord = chkWholeWord.Checked;
			Settings.Default.FileReplace_chkDistinguishCapitals = chkDistinguishCapitals.Checked;
			Settings.Default.FileReplace_chkTrim = chkTrim.Checked;

			Settings.Default.Save();
		}
	}
}
