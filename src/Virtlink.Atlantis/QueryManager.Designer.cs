﻿namespace Virtlink.Atlantis
{
	partial class QueryManager
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblStoredQueries = new System.Windows.Forms.Label();
			this.lstQueries = new System.Windows.Forms.ListView();
			this.btnEdit = new System.Windows.Forms.Button();
			this.btnRemove = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.colName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colQuery = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.colReplacement = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.btnUse = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblStoredQueries
			// 
			this.lblStoredQueries.AutoSize = true;
			this.lblStoredQueries.Location = new System.Drawing.Point(12, 9);
			this.lblStoredQueries.Name = "lblStoredQueries";
			this.lblStoredQueries.Size = new System.Drawing.Size(101, 13);
			this.lblStoredQueries.TabIndex = 0;
			this.lblStoredQueries.Text = "Your saved queries:";
			// 
			// lstQueries
			// 
			this.lstQueries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lstQueries.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName,
            this.colQuery,
            this.colReplacement});
			this.lstQueries.Location = new System.Drawing.Point(12, 25);
			this.lstQueries.Name = "lstQueries";
			this.lstQueries.Size = new System.Drawing.Size(426, 262);
			this.lstQueries.TabIndex = 1;
			this.lstQueries.UseCompatibleStateImageBehavior = false;
			this.lstQueries.View = System.Windows.Forms.View.Details;
			// 
			// btnEdit
			// 
			this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnEdit.Location = new System.Drawing.Point(444, 61);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new System.Drawing.Size(98, 30);
			this.btnEdit.TabIndex = 2;
			this.btnEdit.Text = "&Edit";
			this.btnEdit.UseVisualStyleBackColor = true;
			// 
			// btnRemove
			// 
			this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRemove.Location = new System.Drawing.Point(444, 97);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(98, 30);
			this.btnRemove.TabIndex = 3;
			this.btnRemove.Text = "&Remove";
			this.btnRemove.UseVisualStyleBackColor = true;
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Location = new System.Drawing.Point(444, 258);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(98, 29);
			this.btnClose.TabIndex = 4;
			this.btnClose.Text = "&Close";
			this.btnClose.UseVisualStyleBackColor = true;
			// 
			// colName
			// 
			this.colName.Text = "Name";
			this.colName.Width = 219;
			// 
			// colQuery
			// 
			this.colQuery.Text = "Query";
			this.colQuery.Width = 88;
			// 
			// colReplacement
			// 
			this.colReplacement.Text = "Replacement";
			this.colReplacement.Width = 87;
			// 
			// btnUse
			// 
			this.btnUse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnUse.Location = new System.Drawing.Point(444, 25);
			this.btnUse.Name = "btnUse";
			this.btnUse.Size = new System.Drawing.Size(98, 30);
			this.btnUse.TabIndex = 5;
			this.btnUse.Text = "&Use";
			this.btnUse.UseVisualStyleBackColor = true;
			// 
			// QueryManager
			// 
			this.AcceptButton = this.btnUse;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(554, 299);
			this.Controls.Add(this.btnUse);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnRemove);
			this.Controls.Add(this.btnEdit);
			this.Controls.Add(this.lstQueries);
			this.Controls.Add(this.lblStoredQueries);
			this.Name = "QueryManager";
			this.Text = "Manage queries";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblStoredQueries;
		private System.Windows.Forms.ListView lstQueries;
		private System.Windows.Forms.ColumnHeader colName;
		private System.Windows.Forms.ColumnHeader colQuery;
		private System.Windows.Forms.ColumnHeader colReplacement;
		private System.Windows.Forms.Button btnEdit;
		private System.Windows.Forms.Button btnRemove;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Button btnUse;
	}
}