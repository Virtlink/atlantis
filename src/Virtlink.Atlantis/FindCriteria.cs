﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Virtlink.Atlantis
{
	/// <summary>
	/// Specifies the find criteria.
	/// </summary>
	[Flags]
	public enum FindCriteria
	{
		/// <summary>
		/// No criteria.
		/// </summary>
		None = 0,
		/// <summary>
		/// Find only the whole separated expression.
		/// </summary>
		Whole = 1 << 0, // TODO: Better name.
		/// <summary>
		/// Find only exact capitalized.
		/// </summary>
		CaseSensitive = 1 << 1,
	}
}
