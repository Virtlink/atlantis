﻿using Microsoft.Office.Interop.Word;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Virtlink.Atlantis.Properties;

namespace Virtlink.Atlantis
{
	public partial class Search : Form
	{
		private IReplace query;

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Search"/> class.
		/// </summary>
		public Search()
		{
			InitializeComponent();
			QueryChanged();
			UpdateRegexButtons();
		}
		#endregion

		#region Events
		private void btnFindForward_Click(object sender, EventArgs e)
		{
			EnterShortOperation("Zoeken");

			bool success = WordSearch.FindNext(EnsureQuery(), chkWrapAround.Checked);
			if (success)
				SetStatus(true, "Gevonden.");
			else
				SetStatus(false, "Niet gevonden.");

			LeaveShortOperation();
		}

		private void btnReplace_Click(object sender, EventArgs e)
		{
			EnsureQuery();

			EnterShortOperation("Vervangen");

			bool replaceSuccess = WordSearch.Replace(this.query);
			// No matter whether replace succeeded, we'll find the next occurrence.
			bool findSuccess = WordSearch.FindNext(this.query, chkWrapAround.Checked);

			if (replaceSuccess && findSuccess)
				SetStatus(true, "Vervangen: 1 maal. Gevonden.");
			else if (replaceSuccess && !findSuccess)
				SetStatus(true, "Vervangen: 1 maal. Niet gevonden.");
			else if (!replaceSuccess && findSuccess)
				SetStatus(true, "Gevonden.");
			else
				SetStatus(false, "Niets vervangen. Niets gevonden.");

			LeaveShortOperation();
		}

		private void btnReplaceAll_Click(object sender, EventArgs e)
		{
			EnsureQuery();
			ReplaceAll();
		}
		#endregion

		private IReplace EnsureQuery()
		{
			if (this.query == null)
			{
				this.query = new SimpleQuery(
					txtFind.Text,
					txtReplace.Text,
					GetQueryType(),
					chkWholeWord.Checked,
					chkDistinguishCapitals.Checked);
			}
			return this.query;
		}

		private void SetStatus(bool success, string str, params object[] args)
		{
			this.Invoke(new Action(() =>
			{
				lblStatus.Text = String.Format(str, args);
				lblStatus.ForeColor = success ? SystemColors.ControlText : Color.Red;
			}));
		}

		private void txtFind_TextChanged(object sender, EventArgs e)
		{
			QueryChanged();
		}

		private QueryType GetQueryType()
		{
			return chkFindRegularExpression.Checked ? QueryType.RegularExpression : QueryType.Simple;
		}

		private void QueryChanged()
		{
			bool enabled = Utils.IsValidNonEmptySearchQuery(txtFind.Text, GetQueryType());
			btnFindForward.Enabled = enabled;
			btnReplace.Enabled = enabled;
			btnReplaceAll.Enabled = enabled;
			lblStatus.Text = "";
			this.query = null;
		}

		private UndoRecord undoRecord;
		private Range selectedRange;
		private int replaceCount = 0;
		private void ReplaceAll()
		{
			EnterLongOperation("Alles vervangen");

			// Save the current selection.
			this.selectedRange = Globals.ThisAddIn.Application.Selection.Range;

			replaceAllWorker.RunWorkerAsync();
		}

		private void replaceAllWorker_DoWork(object sender, DoWorkEventArgs e)
		{
			replaceCount = 0;
			bool success = WordSearch.FindNext(this.query, false, 0);
			while (success && !replaceAllWorker.CancellationPending)
			{
				bool replaceSuccess = WordSearch.Replace(this.query);
				if (replaceSuccess)
				{
					replaceCount++;
					SetStatus(true, "Vervangen: {0} maal.", replaceCount);
				}
				success = WordSearch.FindNext(this.query, false);
			}
		}

		private void replaceAllWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			// Restore the selection.
			this.selectedRange.Select();

			if (!e.Cancelled)
				SetStatus(true, "Vervangen: {0} maal.", replaceCount);
			else
				SetStatus(true, "Geannuleerd. Vervangen: {0} maal.", replaceCount);

			LeaveLongOperation();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			replaceAllWorker.CancelAsync();
		}

		private void EnterShortOperation(string name)
		{
			this.undoRecord = WordSearch.StartUndo(name);
		}

		private void LeaveShortOperation()
		{
			WordSearch.EndUndo(this.undoRecord);
		}

		private void EnterLongOperation(string name)
		{
			prbProgress.Visible = true;
			btnCancel.Visible = true;
			grpFind.Enabled = false;
			grpReplace.Enabled = false;

			this.undoRecord = WordSearch.StartUndo(name);
		}

		private void LeaveLongOperation()
		{
			this.Invoke(new Action(() =>
			{
				prbProgress.Visible = false;
				btnCancel.Visible = false;
				grpFind.Enabled = true;
				grpReplace.Enabled = true;

				WordSearch.EndUndo(this.undoRecord);
			}));
		}

		private void chkFindRegularExpression_CheckedChanged(object sender, EventArgs e)
		{
			UpdateRegexButtons();
			QueryChanged();
		}

		private void UpdateRegexButtons()
		{
			btnFindRegex.Enabled = chkFindRegularExpression.Checked;
		}

		private void chkWholeWord_CheckedChanged(object sender, EventArgs e)
		{
			QueryChanged();
		}

		private void chkDistinguishCapitals_CheckedChanged(object sender, EventArgs e)
		{
			QueryChanged();
		}

		private void txtReplace_TextChanged(object sender, EventArgs e)
		{
			QueryChanged();
		}

		private void btnFindRegex_Click(object sender, EventArgs e)
		{
			Process.Start("https://regex101.com/");
		}

		private void Search_Load(object sender, EventArgs e)
		{
			LoadSettings();
		}

		private void Search_FormClosing(object sender, FormClosingEventArgs e)
		{
			SaveSettings();
		}

		private void LoadSettings()
		{
			chkWrapAround.Checked = Settings.Default.Search_chkWrapAround;

			txtFind.Text = Settings.Default.Search_txtFind ?? String.Empty;
			chkFindRegularExpression.Checked = Settings.Default.Search_chkFindRegularExpression;
			chkWholeWord.Checked = Settings.Default.Search_chkWholeWord;
			chkDistinguishCapitals.Checked = Settings.Default.Search_chkDistinguishCapitals;

			txtReplace.Text = Settings.Default.Search_txtReplace ?? String.Empty;
		}

		private void SaveSettings()
		{
			Settings.Default.Search_chkWrapAround = chkWrapAround.Checked;
			
			Settings.Default.Search_txtFind = txtFind.Text;
			Settings.Default.Search_chkFindRegularExpression = chkFindRegularExpression.Checked;
			Settings.Default.Search_chkWholeWord = chkWholeWord.Checked;
			Settings.Default.Search_chkDistinguishCapitals = chkDistinguishCapitals.Checked;

			Settings.Default.Search_txtReplace = txtReplace.Text;

			Settings.Default.Save();
		}
	}
}
