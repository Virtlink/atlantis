﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;

namespace Virtlink.Atlantis
{
	public sealed class WordSearch
	{
		public static UndoRecord StartUndo(string name)
		{
			#region Contract
			if (name == null)
				throw new ArgumentNullException(nameof(name));
			#endregion
			
			UndoRecord record = Globals.ThisAddIn.Application.UndoRecord;
			record.StartCustomRecord(name);
			return record;
		}

		public static void EndUndo(UndoRecord record)
		{
			#region Contract
			if (record == null)
				throw new ArgumentNullException(nameof(record));
			#endregion
			
			record.EndCustomRecord();
		}

		public static bool FindNext(IFind query, bool wrapAround)
		{
			if (query == null)
				return false;

			var document = Globals.ThisAddIn.Application.ActiveDocument;
			var selection = Globals.ThisAddIn.Application.Selection;
			if (selection.Document != document)
				// Let's not do find/replace when the active document
				// is not the document with the cursor/selection.
				return false;

			int contentLength = document.Content.End - document.Content.Start;
			int offset = contentLength > 0 ? Math.Min(selection.Range.End, contentLength - 1) : 0;
			return FindNext(query, wrapAround, offset);
		}

		public static bool FindNext(IFind query, bool wrapAround, int offset)
		{
			if (query == null)
				return false;

			var document = Globals.ThisAddIn.Application.ActiveDocument;
			if (document == null)
				return false;
				
			string content = document.Content.Text;
			if (content.Length == 0)
				return false;

			var opt_occurrence = query.FindNext(content, offset);

			if (opt_occurrence == null)
			{
				if (wrapAround)
					return FindNext(query, false, 0);
				else
					return false;
			}

			var occurrence = (Occurrence)opt_occurrence;
			document.Range(occurrence.SelectionOffset, occurrence.SelectionOffset + occurrence.SelectionLength).Select();

			return true;
		}

		public static bool Replace(IReplace query)
		{
			if (query == null)
				return false;

			var document = Globals.ThisAddIn.Application.ActiveDocument;
			if (document == null)
				return false;

			var selection = Globals.ThisAddIn.Application.Selection;
			if (selection.Document != document)
				// Let's not do find/replace when the active document
				// is not the document with the cursor/selection.
				return false;

			string content = document.Content.Text;
			if (content.Length == 0)
				return false;

			var occurrence = new Occurrence(selection.Range.Start, selection.Range.End - selection.Range.Start);
			var opt_occurrence = query.FindNext(content, occurrence.SelectionOffset);

			if (opt_occurrence == null || opt_occurrence.Value != occurrence)
				// Not found.
				return false;

			// Replace.
			// FIXME: Replace re-matches on the selection only, this doesn't work with lookaround.
			var tuple = query.Replace(selection.Text, new Occurrence(0, occurrence.SelectionLength));
			selection.Text = tuple.Item1;

			return true;
		}
	}
}
