﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;

namespace Virtlink.Atlantis
{
	public interface IFind
	{
		/// <summary>
		/// Returns the next occurrence of the search term
		/// in the specified input.
		/// </summary>
		/// <param name="input">The input to search; or <see langword="null"/>.</param>
		/// <param name="offset">The zero-based offset at which to
		/// start the search.</param>
		/// <returns>The occurrence;
		/// or <see langword="null"/> when not found.</returns>
		Occurrence? FindNext(string input, int offset);
	}
}
