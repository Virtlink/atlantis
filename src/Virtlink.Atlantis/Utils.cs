﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Deployment.Application;

namespace Virtlink.Atlantis
{
	public static class Utils
	{
		public static string GetVersion()
		{
			if (ApplicationDeployment.IsNetworkDeployed)
				return ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
			else
				return typeof(Utils).Assembly.GetName().Version.ToString() + " (debug)";
		}

		/// <summary>
		/// Escapes the query.
		/// </summary>
		/// <param name="query">The query.</param>
		/// <returns>The escaped query.</returns>
		public static string EscapeQueryForRegex(string query, bool specialCharacters)
		{
			#region Contract
			if (query == null)
				throw new ArgumentNullException(nameof(query));
			#endregion

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < query.Length; i++)
			{
				char c = query[i];
				switch (c)
				{
					case '[': sb.Append(@"\["); break;
					case '^': sb.Append(@"\^"); break;
					case '$': sb.Append(@"\$"); break;
					case '.': sb.Append(@"\."); break;
					case '|': sb.Append(@"\|"); break;
					case '?': sb.Append(@"\?"); break;
					case '*': sb.Append(@"\*"); break;
					case '+': sb.Append(@"\+"); break;
					case '(': sb.Append(@"\("); break;
					case ')': sb.Append(@"\)"); break;
					case '{': sb.Append(@"\{"); break;
					case '}': sb.Append(@"\}"); break;
					case '\\':
						if (i < query.Length - 1 && specialCharacters)
						{
							i++;
							c = query[i];
							switch (c)
							{
								case '\\': sb.Append('\\'); break;
								case 'r': sb.Append('\r'); break;
								case 'n': sb.Append('\n'); break;
								case 't': sb.Append('\t'); break;
								default: sb.Append(c); break;
							}
						}
						else
							sb.Append(@"\\");
						break;
					default: sb.Append(c); break;
				}
			}

			return sb.ToString();
		}


		public static string EscapeReplacementForRegex(string replacement, bool specialCharacters)
		{
			#region Contract
			if (replacement == null)
				throw new ArgumentNullException(nameof(replacement));
			#endregion

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < replacement.Length; i++)
			{
				char c = replacement[i];
				switch (c)
				{
					case '\\':
						if (i < replacement.Length - 1 && specialCharacters)
						{
							i++;
							c = replacement[i];
							switch (c)
							{
								case '\\': sb.Append('\\'); break;
								case 'r': sb.Append('\r'); break;
								case 'n': sb.Append('\n'); break;
								case 't': sb.Append('\t'); break;
								default: sb.Append(c); break;
							}
						}
						else
							sb.Append(@"\\");
						break;
					case '$': sb.Append(@"$$"); break;
					default: sb.Append(c); break;
				}
			}

			return sb.ToString();
		}

		/// <summary>
		/// Adds some functionality to the replacement string.
		/// </summary>
		/// <param name="replacement"></param>
		/// <returns></returns>
		public static string ExtendReplacementForRegex(string replacement)
		{
			#region Contract
			if (replacement == null)
				throw new ArgumentNullException(nameof(replacement));
			#endregion

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < replacement.Length; i++)
			{
				char c = replacement[i];
				if (c == '\\' && i < replacement.Length - 1)
				{
					i++;
					c = replacement[i];
					switch (c)
					{
						case '\\': sb.Append('\\'); break;
						case '$': sb.Append("$$"); break;
						// TODO: Support \x[0-9A-F]{1,4}
						// TODO: Support \u[0-9A-F]{4}
						case 'n': sb.Append('\n'); break;
						case 'r': sb.Append('\r'); break;
						case 't': sb.Append('\t'); break;
						case '&': sb.Append("$&"); break;
						case '`': sb.Append("$`"); break;
						case '\'': sb.Append("$'"); break;
						case '_': sb.Append("$_"); break;
						default:
							if (Char.IsDigit(c))
							{
								sb.Append('$').Append(replacement[i]);
								if (i < replacement.Length - 1 && Char.IsDigit(replacement[i + 1]))
								{
									sb.Append(replacement[i + 1]);
									i++;
								}
							}
							else
								sb.Append('\\').Append(c); break;
					}
				}
				else
					sb.Append(c);
			}
			return sb.ToString();
		}

		public static string EscapeForWholeWord(string pattern)
		{
			#region Contract
			if (pattern == null)
				throw new ArgumentNullException(nameof(pattern));
			#endregion
			
			if (!pattern.StartsWith("^"))
				//pattern = @"(?<!\w)" + pattern;
				pattern = "\\b" + pattern;
			if (!pattern.EndsWith("$"))
				//pattern = pattern + @"(?!\w)";
				pattern = pattern + "\\b";
			return pattern;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="queryString"></param>
		/// <param name="type"></param>
		/// <param name="matchWholeWords"></param>
		/// <param name="distinguishCapitals"></param>
		/// <returns>The regex, or <see langword="null"/> when the query string is invalid.</returns>
		public static Regex CreateRegex(string queryString, QueryType type, bool matchWholeWords, bool distinguishCapitals)
		{
			var options = RegexOptions.None;

			string pattern;
			switch (type)
			{
				case QueryType.Simple:
					pattern = Utils.EscapeQueryForRegex(queryString, true);
					break;
				case QueryType.RegularExpression:
					pattern = queryString;
					break;
				default: throw new InvalidOperationException();
			}

			if (!distinguishCapitals)
				options |= RegexOptions.IgnoreCase;

			if (matchWholeWords)
				pattern = Utils.EscapeForWholeWord(pattern);

			try
			{
				return new Regex(pattern, options);
			} catch (ArgumentException)
			{
				// Format error.
				return null;
			}
		}

		public static string CreateReplacement(string replacementString, QueryType type)
		{
			string replacement;
			switch (type)
			{
				case QueryType.Simple:
					replacement = Utils.EscapeReplacementForRegex(replacementString, true);
					break;
				case QueryType.RegularExpression:
					replacement = Utils.ExtendReplacementForRegex(replacementString);
					break;
				default: throw new InvalidOperationException();
			}
			return replacement;
		}

		public static bool IsValidNonEmptySearchQuery(string queryString, QueryType type)
		{
			var regex = CreateRegex(queryString, type, false, false);
			if (regex == null)
				return false;
			return !regex.IsMatch(String.Empty);
		}
	}
}
