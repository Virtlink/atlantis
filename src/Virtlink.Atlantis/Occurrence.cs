﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;

namespace Virtlink.Atlantis
{
	/// <summary>
	/// Where a search term occurs.
	/// </summary>
	public struct Occurrence : IEquatable<Occurrence>
	{
#if false
		/// <summary>
		/// Gets the match offset.
		/// </summary>
		/// <value>The offset of the match.</value>
		public int MatchOffset
		{ get; private set; }


		/// <summary>
		/// Gets the length of the match.
		/// </summary>
		/// <value>The length of the match.</value>
		public int MatchLength
		{ get; private set; }
#endif

		/// <summary>
		/// Gets the selection offset.
		/// </summary>
		/// <value>The offset of the selection.</value>
		public int SelectionOffset
		{ get; private set; }
		// MatchOffset <= SelectionOffset

		/// <summary>
		/// Gets the length of the selection.
		/// </summary>
		/// <value>The length of the selection.</value>
		public int SelectionLength
		{ get; private set; }
		// SelectionLength <= MatchLength

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Occurrence"/> class.
		/// </summary>
		public Occurrence(int offset, int length)
			: this()
		{
			this.SelectionOffset = offset;
			this.SelectionLength = length;
		}
		#endregion

		#region Equality
		/// <inheritdoc />
		public bool Equals(Occurrence other)
		{
			return this.SelectionOffset == other.SelectionOffset
				&& this.SelectionLength == other.SelectionLength;
		}

		/// <inheritdoc />
		public override int GetHashCode()
		{
			int hash = 17;
			unchecked
			{
				hash = hash * 29 + this.SelectionOffset.GetHashCode();
				hash = hash * 29 + this.SelectionLength.GetHashCode();
			}
			return hash;
		}

		/// <inheritdoc />
		public override bool Equals(object obj)
		{
			if (!(obj is Occurrence))
				return false;
			return Equals((Occurrence)obj);
		}

		/// <summary>
		/// Returns a value that indicates whether two specified <see cref="Occurrence"/> objects are equal.
		/// </summary>
		/// <param name="left">The first object to compare.</param>
		/// <param name="right">The second object to compare.</param>
		/// <returns><see langword="true"/> if <paramref name="left"/> and <paramref name="right"/> are equal;
		/// otherwise, <see langword="false"/>.</returns>
		public static bool operator ==(Occurrence left, Occurrence right)
		{
			return Object.Equals(left, right);
		}

		/// <summary>
		/// Returns a value that indicates whether two specified <see cref="Occurrence"/> objects are not equal.
		/// </summary>
		/// <param name="left">The first object to compare.</param>
		/// <param name="right">The second object to compare.</param>
		/// <returns><see langword="true"/> if <paramref name="left"/> and <paramref name="right"/> are not equal;
		/// otherwise, <see langword="false"/>.</returns>
		public static bool operator !=(Occurrence left, Occurrence right)
		{
			return !(left == right);
		}
		#endregion
	}
}
