﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Virtlink.Atlantis
{
	public static class WordUtils
	{

		public static WordVersion GetWordVersion()
		{
			// From here: http://stackoverflow.com/a/3267832/146622

			var version = new Version(Globals.ThisAddIn.Application.Version);
			switch(version.Major)
			{
				case 12: return WordVersion.Word2007;
				case 14: return WordVersion.Word2010;
				case 15: return WordVersion.Word2013;
				default:
					return WordVersion.Unknown;
			}


		}

		public static NativeWindow GetWordWindow()
		{
			var window = new NativeWindow();
			if (GetWordVersion() == WordVersion.Word2013)
			{
				// Supported in Word 2013 and up.
				window.AssignHandle(new IntPtr(Globals.ThisAddIn.Application.ActiveWindow.Hwnd));
			}
			else
			{

				//System.Windows.Forms.NativeWindow mainWindow = new NativeWindow();
				window.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

#if false
				// From here: http://stackoverflow.com/a/8673958/146622
				const string tmp_caption = "Atlantis HWND determination algorithm";

				var word = Globals.ThisAddIn.Application;
				word.Visible = true;
				word.Activate();
				string caption = word.Application.Caption;
				word.Application.Caption = tmp_caption;

				int? handle = null;

				foreach (Process p in Process.GetProcessesByName("winword"))
				{
					if (p.MainWindowTitle.Contains(tmp_caption))
					{
						handle = p.Handle.ToInt32();
						break;
					}
				}

				word.Application.Caption = caption;

				if (handle == null)
					throw new InvalidOperationException("No handle found!");

				return (int)handle;
#endif
			}
			return window;
		}
	}
}
