﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Virtlink.Atlantis
{
	/// <summary>
	/// A simple query.
	/// </summary>
	public sealed class SimpleQuery : IFind, IReplace
	{
		private Regex regex;
		private string replacement;

		/// <summary>
		/// Gets the type of query string.
		/// </summary>
		public QueryType Type
		{ get; private set; }

		/// <summary>
		/// Gets whether to match the query string as a separate word.
		/// </summary>
		public bool MatchWholeWords
		{ get; private set; }

		/// <summary>
		/// Gets whether to match the query string's upper and lower case
		/// letters exactly.
		/// </summary>
		public bool DistinguishCapitals
		{ get; private set; }

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="SimpleQuery"/> class.
		/// </summary>
		public SimpleQuery(string queryString, string replacementString, QueryType type, bool matchWholeWords, bool distinguishCapitals)
		{
			this.regex = Utils.CreateRegex(queryString, type, matchWholeWords, distinguishCapitals);
			if (this.regex == null)
				throw new InvalidOperationException("Invalid query.");
			this.replacement = Utils.CreateReplacement(replacementString, type);
		}
		#endregion

		/// <inheritdoc />
		public Occurrence? FindNext(string input, int offset)
		{
			#region Contract
			if (offset >= input.Length) throw new ArgumentOutOfRangeException(nameof(offset));
			#endregion

			if (String.IsNullOrEmpty(input))
				return null;

			Match result = this.regex.Match(input, offset);
			if (!result.Success)
				return null;
			return new Occurrence(result.Index, result.Length);
		}

		/// <inheritdoc />
		public Tuple<string, Occurrence> Replace(string input, Occurrence occurrence)
		{
			#region Contract
			if (occurrence.SelectionOffset + occurrence.SelectionLength > input.Length)
				throw new ArgumentOutOfRangeException(nameof(occurrence));
			#endregion

			if (String.IsNullOrEmpty(input))
				return null;

			string output = this.regex.Replace(input, replacement, 1);
			
			return Tuple.Create(output, new Occurrence(occurrence.SelectionOffset, occurrence.SelectionLength + (output.Length - input.Length)));
		}
	}
}
