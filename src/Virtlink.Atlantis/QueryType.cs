﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Virtlink.Atlantis
{
	/// <summary>
	/// Specifies the type of query.
	/// </summary>
	public enum QueryType
	{
		/// <summary>
		/// The query is a simple text search.
		/// </summary>
		Simple,
		/// <summary>
		/// The query is a regular expression.
		/// </summary>
		RegularExpression,
	}
}
