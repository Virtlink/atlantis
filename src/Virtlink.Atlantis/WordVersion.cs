﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Virtlink.Atlantis
{
	public enum WordVersion
	{
		Unknown = 0,
		Word2007,
		Word2010,
		Word2013,
	}
}
