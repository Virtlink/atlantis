﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Virtlink.Atlantis
{
	/// <summary>
	/// A stored query.
	/// </summary>
	public sealed class StoredQuery
	{
		/// <summary>
		/// Gets the name of the query.
		/// </summary>
		/// <value>The name of the query.</value>
		public string Name
		{ get; private set; }

		/// <summary>
		/// Gets the search query.
		/// </summary>
		/// <value>The search query.</value>
		public string Find
		{ get; private set; }

		/// <summary>
		/// Gets the replacement text.
		/// </summary>
		/// <value>The replacement text; or <see langword="null"/> when not specified.</value>
		public string Replace
		{ get; private set; }

		/// <summary>
		/// Gets the type of search query.
		/// </summary>
		/// <value>A member of the <see cref="QueryType"/> enumeration.</value>
		public QueryType FindType
		{ get; private set; }

		/// <summary>
		/// Gets the type of replacement.
		/// </summary>
		/// <value>A member of the <see cref="QueryType"/> enumeration.</value>
		public QueryType ReplaceType
		{ get; private set; }

		/// <summary>
		/// Gets the find criteria.
		/// </summary>
		/// <value>A bitwise combination of members of the <see cref="FindCriteria"/> enumeration.</value>
		public FindCriteria FindCriteria
		{ get; private set; }

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="StoredQuery"/> class.
		/// </summary>
		public StoredQuery(
			string name,
			string find,
			string replace,
			QueryType findType,
			QueryType replaceType,
			FindCriteria findCriteria)
		{
			#region Contract
			if (name == null) throw new ArgumentNullException(nameof(name));
			if (find == null) throw new ArgumentNullException(nameof(find));
			if (!Enum.IsDefined(typeof(QueryType), findType)) throw new InvalidEnumArgumentException(nameof(findType), (int)findType, typeof(QueryType));
			if (!Enum.IsDefined(typeof(QueryType), replaceType)) throw new InvalidEnumArgumentException(nameof(replaceType), (int)replaceType, typeof(QueryType));
			#endregion

			this.Name = name;
			this.Find = find;
			this.Replace = replace;
			this.FindType = findType;
			this.ReplaceType = replaceType;
			this.FindCriteria = findCriteria;
		}
		#endregion
	}
}
