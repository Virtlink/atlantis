﻿namespace Virtlink.Atlantis
{
	partial class FileReplace
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileReplace));
			this.lblPath = new System.Windows.Forms.Label();
			this.txtPath = new System.Windows.Forms.TextBox();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.dlgOpenFile = new System.Windows.Forms.OpenFileDialog();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.chkRegularExpressions = new System.Windows.Forms.CheckBox();
			this.chkWholeWord = new System.Windows.Forms.CheckBox();
			this.chkDistinguishCapitals = new System.Windows.Forms.CheckBox();
			this.lblSeparator1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.btnReplaceAll = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.chkTrim = new System.Windows.Forms.CheckBox();
			this.stbStatus = new System.Windows.Forms.StatusStrip();
			this.prbProgress = new System.Windows.Forms.ToolStripProgressBar();
			this.btnCancel = new System.Windows.Forms.ToolStripButton();
			this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
			this.replaceAllWorker = new System.ComponentModel.BackgroundWorker();
			this.pnlMain = new System.Windows.Forms.Panel();
			this.stbStatus.SuspendLayout();
			this.pnlMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblPath
			// 
			this.lblPath.AutoSize = true;
			this.lblPath.Location = new System.Drawing.Point(6, 12);
			this.lblPath.Name = "lblPath";
			this.lblPath.Size = new System.Drawing.Size(144, 13);
			this.lblPath.TabIndex = 0;
			this.lblPath.Text = "Bestand met vervangingslijst:";
			// 
			// txtPath
			// 
			this.txtPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtPath.Location = new System.Drawing.Point(156, 9);
			this.txtPath.Name = "txtPath";
			this.txtPath.Size = new System.Drawing.Size(258, 20);
			this.txtPath.TabIndex = 1;
			this.txtPath.TextChanged += new System.EventHandler(this.txtPath_TextChanged);
			// 
			// btnBrowse
			// 
			this.btnBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnBrowse.Location = new System.Drawing.Point(420, 7);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.Size = new System.Drawing.Size(75, 23);
			this.btnBrowse.TabIndex = 2;
			this.btnBrowse.Text = "Bladeren";
			this.btnBrowse.UseVisualStyleBackColor = true;
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// dlgOpenFile
			// 
			this.dlgOpenFile.FileName = "lijst.txt";
			this.dlgOpenFile.Filter = "Tekstbestand (*.txt)|*.txt|Alle bestanden (*.*)|*.*";
			this.dlgOpenFile.Title = "Bestand met vervangingslijst";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(6, 139);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(489, 46);
			this.label1.TabIndex = 3;
			this.label1.Text = resources.GetString("label1.Text");
			// 
			// textBox1
			// 
			this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBox1.Location = new System.Drawing.Point(6, 188);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.ReadOnly = true;
			this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox1.Size = new System.Drawing.Size(489, 120);
			this.textBox1.TabIndex = 4;
			this.textBox1.Text = "raven\r\nraaf\r\n\r\npotato\r\naardappel\r\n\r\ntomato\r\ntomaat";
			// 
			// chkRegularExpressions
			// 
			this.chkRegularExpressions.AutoSize = true;
			this.chkRegularExpressions.Location = new System.Drawing.Point(156, 35);
			this.chkRegularExpressions.Name = "chkRegularExpressions";
			this.chkRegularExpressions.Size = new System.Drawing.Size(123, 17);
			this.chkRegularExpressions.TabIndex = 5;
			this.chkRegularExpressions.Text = "Reguliere expressies";
			this.chkRegularExpressions.UseVisualStyleBackColor = true;
			// 
			// chkWholeWord
			// 
			this.chkWholeWord.AutoSize = true;
			this.chkWholeWord.Location = new System.Drawing.Point(156, 58);
			this.chkWholeWord.Name = "chkWholeWord";
			this.chkWholeWord.Size = new System.Drawing.Size(75, 17);
			this.chkWholeWord.TabIndex = 6;
			this.chkWholeWord.Text = "Losstaand";
			this.chkWholeWord.UseVisualStyleBackColor = true;
			// 
			// chkDistinguishCapitals
			// 
			this.chkDistinguishCapitals.AutoSize = true;
			this.chkDistinguishCapitals.Location = new System.Drawing.Point(156, 81);
			this.chkDistinguishCapitals.Name = "chkDistinguishCapitals";
			this.chkDistinguishCapitals.Size = new System.Drawing.Size(143, 17);
			this.chkDistinguishCapitals.TabIndex = 7;
			this.chkDistinguishCapitals.Text = "Kapitalen onderscheiden";
			this.chkDistinguishCapitals.UseVisualStyleBackColor = true;
			// 
			// lblSeparator1
			// 
			this.lblSeparator1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lblSeparator1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblSeparator1.Location = new System.Drawing.Point(6, 127);
			this.lblSeparator1.Name = "lblSeparator1";
			this.lblSeparator1.Size = new System.Drawing.Size(489, 2);
			this.lblSeparator1.TabIndex = 8;
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label2.Location = new System.Drawing.Point(6, 320);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(489, 2);
			this.label2.TabIndex = 9;
			// 
			// btnReplaceAll
			// 
			this.btnReplaceAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnReplaceAll.Location = new System.Drawing.Point(378, 325);
			this.btnReplaceAll.Name = "btnReplaceAll";
			this.btnReplaceAll.Size = new System.Drawing.Size(117, 23);
			this.btnReplaceAll.TabIndex = 10;
			this.btnReplaceAll.Text = "Alles vervangen";
			this.btnReplaceAll.UseVisualStyleBackColor = true;
			this.btnReplaceAll.Click += new System.EventHandler(this.btnReplaceAll_Click);
			// 
			// btnClose
			// 
			this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClose.Location = new System.Drawing.Point(255, 325);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(117, 23);
			this.btnClose.TabIndex = 11;
			this.btnClose.Text = "Sluiten";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// chkTrim
			// 
			this.chkTrim.AutoSize = true;
			this.chkTrim.Location = new System.Drawing.Point(156, 104);
			this.chkTrim.Name = "chkTrim";
			this.chkTrim.Size = new System.Drawing.Size(290, 17);
			this.chkTrim.TabIndex = 12;
			this.chkTrim.Text = "Witruimte om zoek- en vervangingswoorden verwijderen";
			this.chkTrim.UseVisualStyleBackColor = true;
			// 
			// stbStatus
			// 
			this.stbStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prbProgress,
            this.btnCancel,
            this.lblStatus});
			this.stbStatus.Location = new System.Drawing.Point(0, 358);
			this.stbStatus.Name = "stbStatus";
			this.stbStatus.Size = new System.Drawing.Size(506, 22);
			this.stbStatus.TabIndex = 13;
			this.stbStatus.Text = "statusStrip1";
			// 
			// prbProgress
			// 
			this.prbProgress.MarqueeAnimationSpeed = 50;
			this.prbProgress.Name = "prbProgress";
			this.prbProgress.Size = new System.Drawing.Size(100, 16);
			this.prbProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.prbProgress.Visible = false;
			// 
			// btnCancel
			// 
			this.btnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
			this.btnCancel.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(66, 20);
			this.btnCancel.Text = "Annuleren";
			this.btnCancel.Visible = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lblStatus
			// 
			this.lblStatus.Name = "lblStatus";
			this.lblStatus.Size = new System.Drawing.Size(39, 17);
			this.lblStatus.Text = "Status";
			// 
			// replaceAllWorker
			// 
			this.replaceAllWorker.WorkerSupportsCancellation = true;
			this.replaceAllWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.replaceAllWorker_DoWork);
			this.replaceAllWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.replaceAllWorker_RunWorkerCompleted);
			// 
			// pnlMain
			// 
			this.pnlMain.Controls.Add(this.chkTrim);
			this.pnlMain.Controls.Add(this.label2);
			this.pnlMain.Controls.Add(this.btnClose);
			this.pnlMain.Controls.Add(this.lblSeparator1);
			this.pnlMain.Controls.Add(this.btnReplaceAll);
			this.pnlMain.Controls.Add(this.chkDistinguishCapitals);
			this.pnlMain.Controls.Add(this.chkWholeWord);
			this.pnlMain.Controls.Add(this.chkRegularExpressions);
			this.pnlMain.Controls.Add(this.textBox1);
			this.pnlMain.Controls.Add(this.label1);
			this.pnlMain.Controls.Add(this.btnBrowse);
			this.pnlMain.Controls.Add(this.txtPath);
			this.pnlMain.Controls.Add(this.lblPath);
			this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pnlMain.Location = new System.Drawing.Point(0, 0);
			this.pnlMain.Name = "pnlMain";
			this.pnlMain.Size = new System.Drawing.Size(506, 358);
			this.pnlMain.TabIndex = 14;
			// 
			// FileReplace
			// 
			this.AcceptButton = this.btnReplaceAll;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnClose;
			this.ClientSize = new System.Drawing.Size(506, 380);
			this.Controls.Add(this.pnlMain);
			this.Controls.Add(this.stbStatus);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FileReplace";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Meervoudig vervangen";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FileReplace_FormClosing);
			this.Load += new System.EventHandler(this.FileReplace_Load);
			this.stbStatus.ResumeLayout(false);
			this.stbStatus.PerformLayout();
			this.pnlMain.ResumeLayout(false);
			this.pnlMain.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblPath;
		private System.Windows.Forms.TextBox txtPath;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.OpenFileDialog dlgOpenFile;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.CheckBox chkRegularExpressions;
		private System.Windows.Forms.CheckBox chkWholeWord;
		private System.Windows.Forms.CheckBox chkDistinguishCapitals;
		private System.Windows.Forms.Label lblSeparator1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button btnReplaceAll;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.CheckBox chkTrim;
		private System.Windows.Forms.StatusStrip stbStatus;
		private System.Windows.Forms.ToolStripStatusLabel lblStatus;
		private System.Windows.Forms.ToolStripProgressBar prbProgress;
		private System.Windows.Forms.ToolStripButton btnCancel;
		private System.ComponentModel.BackgroundWorker replaceAllWorker;
		private System.Windows.Forms.Panel pnlMain;
	}
}