﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Virtlink.Atlantis
{
	/// <summary>
	/// Some Win32 window.
	/// </summary>
	public sealed class Win32Window : IWin32Window
	{
		/// <inheritdoc />
		public IntPtr Handle
		{ get; private set; }

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Win32Window"/> class.
		/// </summary>
		/// <param name="handle">The handle.</param>
		public Win32Window(int handle)
			: this(new IntPtr(handle))
		{ /* Nothing to do. */ }

		/// <summary>
		/// Initializes a new instance of the <see cref="Win32Window"/> class.
		/// </summary>
		/// <param name="handle">The handle.</param>
		public Win32Window(IntPtr handle)
		{
			this.Handle = handle;
		}
		#endregion
	}
}
