﻿namespace Virtlink.Atlantis
{
	partial class Search
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Search));
			this.lblFind = new System.Windows.Forms.Label();
			this.txtFind = new System.Windows.Forms.TextBox();
			this.btnFindForward = new System.Windows.Forms.Button();
			this.txtReplace = new System.Windows.Forms.TextBox();
			this.btnReplace = new System.Windows.Forms.Button();
			this.btnReplaceAll = new System.Windows.Forms.Button();
			this.chkInSelection = new System.Windows.Forms.CheckBox();
			this.grpFind = new System.Windows.Forms.GroupBox();
			this.btnFindRegex = new System.Windows.Forms.Button();
			this.chkWrapAround = new System.Windows.Forms.CheckBox();
			this.chkDistinguishCapitals = new System.Windows.Forms.CheckBox();
			this.chkWholeWord = new System.Windows.Forms.CheckBox();
			this.chkFindRegularExpression = new System.Windows.Forms.CheckBox();
			this.grpReplace = new System.Windows.Forms.GroupBox();
			this.btnReplaceRegex = new System.Windows.Forms.Button();
			this.lblReplace = new System.Windows.Forms.Label();
			this.stbStatus = new System.Windows.Forms.StatusStrip();
			this.prbProgress = new System.Windows.Forms.ToolStripProgressBar();
			this.btnCancel = new System.Windows.Forms.ToolStripButton();
			this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
			this.replaceAllWorker = new System.ComponentModel.BackgroundWorker();
			this.grpFind.SuspendLayout();
			this.grpReplace.SuspendLayout();
			this.stbStatus.SuspendLayout();
			this.SuspendLayout();
			// 
			// lblFind
			// 
			resources.ApplyResources(this.lblFind, "lblFind");
			this.lblFind.Name = "lblFind";
			// 
			// txtFind
			// 
			resources.ApplyResources(this.txtFind, "txtFind");
			this.txtFind.Name = "txtFind";
			this.txtFind.TextChanged += new System.EventHandler(this.txtFind_TextChanged);
			// 
			// btnFindForward
			// 
			resources.ApplyResources(this.btnFindForward, "btnFindForward");
			this.btnFindForward.Name = "btnFindForward";
			this.btnFindForward.UseVisualStyleBackColor = true;
			this.btnFindForward.Click += new System.EventHandler(this.btnFindForward_Click);
			// 
			// txtReplace
			// 
			resources.ApplyResources(this.txtReplace, "txtReplace");
			this.txtReplace.Name = "txtReplace";
			this.txtReplace.TextChanged += new System.EventHandler(this.txtReplace_TextChanged);
			// 
			// btnReplace
			// 
			resources.ApplyResources(this.btnReplace, "btnReplace");
			this.btnReplace.Name = "btnReplace";
			this.btnReplace.UseVisualStyleBackColor = true;
			this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
			// 
			// btnReplaceAll
			// 
			resources.ApplyResources(this.btnReplaceAll, "btnReplaceAll");
			this.btnReplaceAll.Name = "btnReplaceAll";
			this.btnReplaceAll.UseVisualStyleBackColor = true;
			this.btnReplaceAll.Click += new System.EventHandler(this.btnReplaceAll_Click);
			// 
			// chkInSelection
			// 
			resources.ApplyResources(this.chkInSelection, "chkInSelection");
			this.chkInSelection.Name = "chkInSelection";
			this.chkInSelection.UseVisualStyleBackColor = true;
			// 
			// grpFind
			// 
			resources.ApplyResources(this.grpFind, "grpFind");
			this.grpFind.Controls.Add(this.btnFindRegex);
			this.grpFind.Controls.Add(this.chkWrapAround);
			this.grpFind.Controls.Add(this.chkDistinguishCapitals);
			this.grpFind.Controls.Add(this.chkWholeWord);
			this.grpFind.Controls.Add(this.chkFindRegularExpression);
			this.grpFind.Controls.Add(this.lblFind);
			this.grpFind.Controls.Add(this.txtFind);
			this.grpFind.Controls.Add(this.btnFindForward);
			this.grpFind.Name = "grpFind";
			this.grpFind.TabStop = false;
			// 
			// btnFindRegex
			// 
			resources.ApplyResources(this.btnFindRegex, "btnFindRegex");
			this.btnFindRegex.Name = "btnFindRegex";
			this.btnFindRegex.UseVisualStyleBackColor = true;
			this.btnFindRegex.Click += new System.EventHandler(this.btnFindRegex_Click);
			// 
			// chkWrapAround
			// 
			resources.ApplyResources(this.chkWrapAround, "chkWrapAround");
			this.chkWrapAround.Name = "chkWrapAround";
			this.chkWrapAround.UseVisualStyleBackColor = true;
			// 
			// chkDistinguishCapitals
			// 
			resources.ApplyResources(this.chkDistinguishCapitals, "chkDistinguishCapitals");
			this.chkDistinguishCapitals.Name = "chkDistinguishCapitals";
			this.chkDistinguishCapitals.UseVisualStyleBackColor = true;
			this.chkDistinguishCapitals.CheckedChanged += new System.EventHandler(this.chkDistinguishCapitals_CheckedChanged);
			// 
			// chkWholeWord
			// 
			resources.ApplyResources(this.chkWholeWord, "chkWholeWord");
			this.chkWholeWord.Name = "chkWholeWord";
			this.chkWholeWord.UseVisualStyleBackColor = true;
			this.chkWholeWord.CheckedChanged += new System.EventHandler(this.chkWholeWord_CheckedChanged);
			// 
			// chkFindRegularExpression
			// 
			resources.ApplyResources(this.chkFindRegularExpression, "chkFindRegularExpression");
			this.chkFindRegularExpression.Name = "chkFindRegularExpression";
			this.chkFindRegularExpression.UseVisualStyleBackColor = true;
			this.chkFindRegularExpression.CheckedChanged += new System.EventHandler(this.chkFindRegularExpression_CheckedChanged);
			// 
			// grpReplace
			// 
			resources.ApplyResources(this.grpReplace, "grpReplace");
			this.grpReplace.Controls.Add(this.btnReplaceRegex);
			this.grpReplace.Controls.Add(this.lblReplace);
			this.grpReplace.Controls.Add(this.chkInSelection);
			this.grpReplace.Controls.Add(this.txtReplace);
			this.grpReplace.Controls.Add(this.btnReplaceAll);
			this.grpReplace.Controls.Add(this.btnReplace);
			this.grpReplace.Name = "grpReplace";
			this.grpReplace.TabStop = false;
			// 
			// btnReplaceRegex
			// 
			resources.ApplyResources(this.btnReplaceRegex, "btnReplaceRegex");
			this.btnReplaceRegex.Name = "btnReplaceRegex";
			this.btnReplaceRegex.UseVisualStyleBackColor = true;
			// 
			// lblReplace
			// 
			resources.ApplyResources(this.lblReplace, "lblReplace");
			this.lblReplace.Name = "lblReplace";
			// 
			// stbStatus
			// 
			this.stbStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.prbProgress,
            this.btnCancel,
            this.lblStatus});
			resources.ApplyResources(this.stbStatus, "stbStatus");
			this.stbStatus.Name = "stbStatus";
			// 
			// prbProgress
			// 
			this.prbProgress.MarqueeAnimationSpeed = 50;
			this.prbProgress.Name = "prbProgress";
			resources.ApplyResources(this.prbProgress, "prbProgress");
			this.prbProgress.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			// 
			// btnCancel
			// 
			this.btnCancel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			resources.ApplyResources(this.btnCancel, "btnCancel");
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// lblStatus
			// 
			this.lblStatus.Name = "lblStatus";
			resources.ApplyResources(this.lblStatus, "lblStatus");
			// 
			// replaceAllWorker
			// 
			this.replaceAllWorker.WorkerSupportsCancellation = true;
			this.replaceAllWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.replaceAllWorker_DoWork);
			this.replaceAllWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.replaceAllWorker_RunWorkerCompleted);
			// 
			// Search
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.stbStatus);
			this.Controls.Add(this.grpReplace);
			this.Controls.Add(this.grpFind);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Search";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Search_FormClosing);
			this.Load += new System.EventHandler(this.Search_Load);
			this.grpFind.ResumeLayout(false);
			this.grpFind.PerformLayout();
			this.grpReplace.ResumeLayout(false);
			this.grpReplace.PerformLayout();
			this.stbStatus.ResumeLayout(false);
			this.stbStatus.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblFind;
		private System.Windows.Forms.TextBox txtFind;
		private System.Windows.Forms.Button btnFindForward;
		private System.Windows.Forms.TextBox txtReplace;
		private System.Windows.Forms.Button btnReplace;
		private System.Windows.Forms.Button btnReplaceAll;
		private System.Windows.Forms.CheckBox chkInSelection;
		private System.Windows.Forms.GroupBox grpFind;
		private System.Windows.Forms.GroupBox grpReplace;
		private System.Windows.Forms.Label lblReplace;
		private System.Windows.Forms.CheckBox chkFindRegularExpression;
		private System.Windows.Forms.CheckBox chkDistinguishCapitals;
		private System.Windows.Forms.CheckBox chkWholeWord;
		private System.Windows.Forms.CheckBox chkWrapAround;
		private System.Windows.Forms.StatusStrip stbStatus;
		private System.Windows.Forms.ToolStripStatusLabel lblStatus;
		private System.Windows.Forms.ToolStripProgressBar prbProgress;
		private System.Windows.Forms.ToolStripButton btnCancel;
		private System.ComponentModel.BackgroundWorker replaceAllWorker;
		private System.Windows.Forms.Button btnFindRegex;
		private System.Windows.Forms.Button btnReplaceRegex;
	}
}